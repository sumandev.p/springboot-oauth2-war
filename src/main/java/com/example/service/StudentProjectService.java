package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.StudentProject;
import com.example.repo.StudentProjectRepository;



@Service
public class StudentProjectService  {
	
	
	private StudentProjectRepository studentProjectRepository;
	
	
	@Autowired
	public StudentProjectService(StudentProjectRepository studentProjectRepository) {
		
		this.studentProjectRepository = studentProjectRepository;
	
	}
	

	  public List<StudentProject> listAll() {
	        List<StudentProject> studentProject = new ArrayList<>();
	        studentProjectRepository.findAll().forEach(studentProject::add); //fun with Java 8
	        return studentProject;
	    }
	    
	
	    public StudentProject getById(Long id) {
	       // return studentProjectRepository.findOne(id).orElse(null);
	        return studentProjectRepository.findOne(id);
	    }
	
	    public StudentProject saveOrUpdate(StudentProject studentProject) {
	        studentProjectRepository.save(studentProject);
	        return studentProject;
	    }
	

	
	public void delete(Long id) {
	    
	     studentProjectRepository.delete(id);
		    
	    }


}

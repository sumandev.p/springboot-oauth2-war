package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.StudentProject;
import com.example.service.StudentProjectService;


@RestController
public class StudentProjectController {
	
	private StudentProjectService studentProjectService;
		
	@Autowired
	public void setStudentProjectService(StudentProjectService studentProjectService) {
		this.studentProjectService = studentProjectService;
	}

	 
	 @RequestMapping({"/studentproject/list", "/studentproject"})
	    public List<StudentProject> listProducts(Model model){
	        model.addAttribute("studentprojects", studentProjectService.listAll());
	        return studentProjectService.listAll();
	    }
	 
	 
	 @RequestMapping("/studentproject/admin/list")
	    public List<StudentProject> listProductsall(){
	        
	        return studentProjectService.listAll();
	    }
	 

	 @RequestMapping("/studentproject/show/{id}")
	    public StudentProject getStudentProject(@PathVariable Long id, Model model){
	        model.addAttribute("product", studentProjectService.getById(Long.valueOf(id)));
	        return studentProjectService.getById(id) ;
	    }
	 
	 
	 @RequestMapping(method = RequestMethod.PUT,value = "/studentproject/{id}")
	    public void updateStudentProject(@RequestBody StudentProject studentProject,@PathVariable Long id) {
		 
		 studentProjectService.saveOrUpdate(studentProject);
		 	    	
	    }
	 
	 
	 
	 @RequestMapping(method = RequestMethod.POST,value = "/studentproject/add")
	    public void addStudentProject(@RequestBody StudentProject studentProject ){
		 
		 studentProjectService.saveOrUpdate(studentProject);
		 
	 }
	 
	 @RequestMapping(method = RequestMethod.DELETE,value ="/studentproject/{id}")
	    public void delete(@PathVariable Long id){
		
	          studentProjectService.delete(id);
	    }
	

}

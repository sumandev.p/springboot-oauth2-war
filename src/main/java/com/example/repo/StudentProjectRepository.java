package com.example.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.model.StudentProject;

public interface StudentProjectRepository extends CrudRepository<StudentProject, Long> {

}
